@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <legend>
                <h1>Add Country - {{ $client->siteUrl }}</h1>
            </legend>
            
            <form method="POST" action="/country/{{$client->id}}/add">
                {{ csrf_field() }}
                <input list="countries" name="country" required class="form-control">

                <datalist id="countries">
                    @foreach($countries as $country)
                    <option value="{{$country->name}}">
                    @endforeach
                  
                </datalist>
                <br>
                <input type="submit" class="btn btn-success" value="Submit">
            </form>
        </div>
    </div>
</div>
@endsection