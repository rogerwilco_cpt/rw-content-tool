<table>
    <tbody>
        @foreach($clients as $client)
        <tr>
            <td>{{ $client->id }}</td>
            <td>{{ $client->siteUrl }}</td>
            <td>{{ parse_url($client->siteUrl, PHP_URL_HOST) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
