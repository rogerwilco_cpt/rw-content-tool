@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <legend>
                <h1>Countries - {{ $client->siteUrl }}</h1>
            </legend>
            
            <a href="/country/{{$client->id}}/add" class="btn btn-success">Add Country</a>
            <br><br>
            
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody class="tableBody">
                    @foreach($client->countries as $country)
                    <tr>
                        <td>{{ $country->name }}</td>
                        <td>
                            <a href="/country/{{$client->id}}/delete/{{ $country->id }}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure you want to remove this country?');">-</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection