@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <legend>
                <h1>{{ $client->siteUrl }}</h1>
            </legend>
            
            <form method="POST" action="/clients/{{ $client->id }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                @method('PUT')
                <input type="file" class="form-control" name="file" accept=".csv" required>
                <br>
                
                <input type="submit" class="btn btn-success" value="Upload Keywords">
            </form>
            <br><br>
            
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Keyword</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($client->keywords()->where('status', 1)->get() as $keyword)
                    <tr>
                        <td>{{ $keyword->keyword }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
