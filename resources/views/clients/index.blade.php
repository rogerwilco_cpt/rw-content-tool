@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <legend>
                <h1>Clients</h1>
            </legend>
            <a href="/clients/create" class="btn btn-success">Import Clients From Search Console</a>
            <hr>
            <a href="/runs/create" class="btn btn-success">Import Search Data</a>
            <hr>
            <!--<form method="POST" action="/settings" class="form-inline">-->
            <!--    {{ csrf_field() }}-->
            <!--    <input type="number" class="form-control" name="days" min="1" step="1" value="{{ $days }}">-->
            <!--    <input type="submit" class="btn btn-success" value="Update Number Of Days Per Run">-->
            <!--    <input type="hidden" name="name" value="days">-->
            <!--</form>-->
            <br>
            <br>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>URL</th>
                        <th>Ownership</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach(\App\Client::orderBy('status', 'asc')->get() as $client)
                    <tr>
                        <td>{{ $client->id }}</td>
                        <td>{{ $client->siteUrl }}</td>
                        <td>{{ $client->permissionLevel }}</td>
                        <td>
                            @if($client->status == 1)
                            <form method="POST" action="/clients/{{$client->id}}">
                                @method('DELETE')
                                {{ csrf_field() }}
                                <input type="submit" value="Active" class="btn btn-success btn-sm">
                            </form>
                            @else
                            <form method="POST" action="/clients/{{$client->id}}">
                                @method('DELETE')
                                {{ csrf_field() }}
                                <input type="submit" value="Inactive" class="btn btn-danger btn-sm">
                            </form>
                            @endif
                        </td>
                        <td>
                            <a href="/country/{{$client->id}}" class="btn btn-primary btn-sm">Edit Country</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
