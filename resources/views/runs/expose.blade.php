<table>
    <tbody>
        <tr>
            <td></td>
            @for($i = 0; $i < count($runs); $i++)
            <td colspan="3">{{ $runs[$i]->start_date }}</td>
            @endfor
        </tr>
        <tr>
            <td>Keyword</td>
            @for($i = 0; $i < count($runs); $i++)
            <td>CTR</td>
            <td>IMP</td>
            <td>SV</td>
            @endfor
        </tr>
        @foreach($table as $keyword => $row)
        <tr>
            <td>{{ $keyword }}</td>
            @for($i = 0; $i < count($runs); $i++)
            <td>{{ $row[$i]['ctr'] or '' }}</td>
            <td>{{ $row[$i]['imp'] or '' }}</td>
            <td>{{ $row[$i]['sv'] or '' }}</td>
            @endfor
        </tr>
        @endforeach
    </tbody>
</table>
