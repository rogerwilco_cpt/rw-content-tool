<?php
namespace App\Traits;

use Google_Client;
use App\Token;

trait TokenAuth
{
    protected $client;
    protected $token;
    // public function auth_request() {
    //     putenv('GOOGLE_APPLICATION_CREDENTIALS='.storage_path('app/google/rw-health-checks-ea773006194d.json'));
    //     $client = new Google_Client();
    //     $client->useApplicationDefaultCredentials();
    //     $client->addScope(\Google_Service_Webmasters::WEBMASTERS);
    //     $httpClient = $client->authorize();
    //     return $httpClient;
    // }
    
    public function check_access_token($client) {
        if($client->isAccessTokenExpired()) {
            return false;
        }
        else {
            return true;
        }
        
    }
    
    public function create_new_client ($token, $scope, $url) {
        $this->client = new Google_Client();
        $this->client->setAuthConfig(storage_path('app/google/client_secret_27733745713-osqscndhvqk7kmfiffqiulun90b1rnh9.apps.googleusercontent.com (1).json'));
        if($token != null) {
            $this->client->setAccessToken($token);
            //$this->auth_client($scope, $url);
            if($this->check_access_token($this->client)) {
                return;
            }
            else {
                $this->auth_client($scope, $url);
            }
        }
        else {
            $this->auth_client($scope, $url);
        }
        
        
    }
    public function auth_client($scope, $url) {
        $this->client->addScope($scope);
        $this->client->setRedirectUri($url);
        $auth_url = $this->client->createAuthUrl();
        exit(redirect($auth_url));
    }
    
    public function get_token_from_db() {
        $token = Token::latest()->first();
        if($token != null) {
            $return['access_token'] = $token->access_token;
            $return['token_type'] = $token->token_type;
            $return['expires_in'] = $token->expires_in;
            $return['created'] = $token->created;
            $this->token = $return;
        }
        return;
    }
    
    public function check_code($request, $scope, $url) {
        if($request->input('code') != NULL) {
            $code = $request->input('code');
            $this->client = new Google_Client();
            $this->client->setAuthConfig(storage_path('app/google/client_secret_27733745713-osqscndhvqk7kmfiffqiulun90b1rnh9.apps.googleusercontent.com (1).json'));
            $this->client->setRedirectUri(url()->current());
            $token = $this->client->fetchAccessTokenWithAuthCode($code);
            //$this->token = $token;
            $db_token = new Token;
            $db_token->access_token = $token['access_token'];
            $db_token->token_type = $token['token_type'];
            $db_token->expires_in = $token['expires_in'];
            $db_token->created = $token['created'];
            $db_token->save();
        }
        else {
            $this->client = new Google_Client();
            $this->client->setAuthConfig(storage_path('app/google/client_secret_27733745713-osqscndhvqk7kmfiffqiulun90b1rnh9.apps.googleusercontent.com (1).json'));
            $this->auth_client($scope, $url);
        }
    }
    
    public function getApiToken() {
        $token = Token::latest()->first();
        $return['access_token'] = $token->access_token;
        $return['token_type'] = $token->token_type;
        $return['expires_in'] = $token->expires_in;
        $return['created'] = $token->created;
        $this->token = $return;
        $this->client = new Google_Client();
        $this->client->setAuthConfig(public_path('/google/rwhc.json'));
        $this->client->setAccessToken($this->token);
    }
    
    // public function service_account() {
    //     putenv('GOOGLE_APPLICATION_CREDENTIALS=/var/www/searchdata/g_cloud_services.json');
    //     $client = new Google_Client();
    //     $client->useApplicationDefaultCredentials();
    //     $client->addScope(\Google_Service_Webmasters::WEBMASTERS);
    //     $client->setSubject('rogerwilco.cpt@gmail.com');
    //     return $client;
    // }
}