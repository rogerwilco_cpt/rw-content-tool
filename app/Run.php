<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Run extends Model
{
    public function alldata()
    {
        return $this->hasMany('App\SearchAllData');
    }
    
    public function data()
    {
        return $this->hasMany('App\SearchData');
    }
}
