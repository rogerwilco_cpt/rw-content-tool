<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Client;
use App\Token;
use Google_Client;
use App\Run;
use App\SearchAllData;
use App\SearchData;

class GetSearchData implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    
    protected $client;
    protected $google_client;
    protected $token;
    protected $start_date;
    protected $end_date;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Client $client, $start_date, $end_date)
    {
        $this->client = $client;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        
        $token = Token::latest()->first();
        $return['access_token'] = $token->access_token;
        $return['token_type'] = $token->token_type;
        $return['expires_in'] = $token->expires_in;
        $return['created'] = $token->created;
        $this->token = $return;
        $this->google_client = new Google_Client();
        $this->google_client->setAuthConfig(storage_path('app/google/client_secret_27733745713-osqscndhvqk7kmfiffqiulun90b1rnh9.apps.googleusercontent.com (1).json'));
        $this->google_client->setAccessToken($this->token);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // $days = (int) \App\Setting::where('name', 'days')->value('value');
        // $days_ago = 2 + $days;
        
        ini_set('max_execution_time', 0);
        $run = new Run;
        $run->start_date = $this->start_date;
        $run->end_date = $this->end_date;
        $this->client->runs()->save($run);
        
        $service = new \Google_Service_Webmasters($this->google_client);
        if(filter_var($this->client->siteUrl, FILTER_VALIDATE_URL) === FALSE){
        	return;
        }
        
        $url = $this->client->siteUrl;
        $search = new \Google_Service_Webmasters_SearchAnalyticsQueryRequest;
        $search->setStartDate( date( 'Y-m-d', strtotime( $this->start_date ) ) );
        $search->setEndDate( date( 'Y-m-d', strtotime( $this->end_date ) ) );
        $search->setDimensions(['query']);
        $search->setRowLimit(25000);
        
        $results = $service->searchanalytics->query( $url, $search )->getRows();
        $results = collect($results);
        
        foreach($results as $result) {
            $data = new SearchAllData;
            $data->keyword = $result->keys[0];
            $data->ctr = $result->ctr;
            $data->impressions = (empty($result->impressions) ? 0 : $result->impressions);
            $data->position = (empty($result->position) ? 0 : $result->position);
            $data->clicks = $result->clicks;
            $data->run_id = $run->id;
            
            $data->sv = 0;
            
            $this->client->alldata()->save($data);
        }
        
        $searchdata = $run->alldata()->whereIn('keyword', $this->client->keywords()->pluck('keyword'))->get();
        
        foreach($searchdata as $row) {
            $data = new SearchData;
            $data->keyword = $row->keyword;
            $data->ctr = $row->ctr;
            $data->impressions = $row->impressions;
            $data->run_id = $row->run_id;
            
            $data->sv = $row->sv;
            
            $this->client->rundata()->save($data);
        }
    }
}
