<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GetSearchVolumes implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    protected $keywords;
    protected $controller;
    protected $countries;
    
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($keywords, $countries)
    {
        $this->keywords = $keywords;
        $this->controller = new \App\Http\Controllers\SearchVolumeController;
        $this->countries = $countries;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->controller->run_adwords_query($this->keywords, $this->countries);
    }
}
