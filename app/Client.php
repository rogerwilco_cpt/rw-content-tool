<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['siteUrl'];
    
    public function keywords()
    {
        return $this->hasMany('App\Keyword');
    }
    
    public function runs()
    {
        return $this->hasMany('App\Run');
    }
    
    public function alldata()
    {
        return $this->hasMany('App\SearchAllData');
    }
    
    public function rundata()
    {
        return $this->hasMany('App\SearchData');
    }
    
    public function countries()
    {
        return $this->belongsToMany('App\Country', 'client_countries');
    }
}
