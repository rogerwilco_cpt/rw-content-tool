<?php

namespace App\Http\Controllers;

use App\Run;
use Illuminate\Http\Request;
use App\Client;
use App\Traits\TokenAuth;
use App\SearchAllData;
use App\SearchData;
use App\Jobs\GetSearchData;
use App\Setting;

class RunController extends Controller
{
    use TokenAuth;
    
    protected $start_date;
    protected $end_date;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // if($request->input('start_date') != NULL) {
        //     $setting = Setting::firstOrNew(['name' => 'start_date']);
        //     $setting->value = $request->input('start_date');
        //     $setting->save();
        //     $setting = Setting::firstOrNew(['name' => 'end_date']);
        //     $setting->value = $request->input('end_date');
        //     $setting->save();
        // }
        
        $this->check_code($request, [\Google_Service_Webmasters::WEBMASTERS_READONLY, \Google_Service_Analytics::ANALYTICS_READONLY], url()->current());
        
        $seconds = 10;
        $this->start_date = '2018-01-01';
        $this->end_date = strtotime('-3 days');
        
        while(strtotime($this->start_date." +13 days") <= $this->end_date) {
            
            $start_date = $this->start_date;
            $end_date = date('Y-m-d', strtotime($this->start_date." +13 days"));
            $this->start_date = date('Y-m-d', strtotime($this->start_date." +14 days"));
            
            
            
            foreach(Client::where('status', 1)->get() as $client) {
                
                if($client->runs()->where('start_date', $start_date)->get()->isEmpty()) {
                    GetSearchData::dispatch($client, $start_date, $end_date)->delay(now()->addSeconds($seconds));
                
                    $seconds = $seconds + 10;
                }
            
                
                
            }
            
        }
        
        
        
        return redirect('/clients')->with('message', 'Data processing in progress');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Run  $run
     * @return \Illuminate\Http\Response
     */
    public function show(Run $run)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Run  $run
     * @return \Illuminate\Http\Response
     */
    public function edit(Run $run)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Run  $run
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Run $run)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Run  $run
     * @return \Illuminate\Http\Response
     */
    public function destroy(Run $run)
    {
        //
    }
}
