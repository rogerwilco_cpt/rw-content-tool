<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use App\Traits\TokenAuth;
use Storage;
use App\Keyword;
use App\SearchAllData;
use DB;

class ClientController extends Controller
{
    use TokenAuth;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $days = (int) \App\Setting::where('name', 'days')->value('value');
        return view('clients.index', ['days' => $days]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->check_code($request, [\Google_Service_Webmasters::WEBMASTERS_READONLY, \Google_Service_Analytics::ANALYTICS_READONLY], url()->current());
        $search = new \Google_Service_Webmasters($this->client);
        $sites = $search->sites->listSites();
        foreach($sites->siteEntry as $entry) {
            $site = Client::firstOrNew(['siteUrl' => $entry->siteUrl]);
            if($entry->permissionLevel != 'siteFullUser' && $entry->permissionLevel != 'siteOwner') {
                continue;
            }
            $site->permissionLevel = $entry->permissionLevel;
            if($site->status != 1) {
                $site->status = 2;
            }
            $site->save();
        }
        return redirect('/clients')->with('message', 'Sites Imported');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('clients.edit', ['client' => $client]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $storage = Storage::putFile('keywords', $request->file('file'));
        
        $contents = Storage::get($storage);
        $keywords = explode("\r\n",$contents);
        
        $client->keywords()->delete();
        
        foreach($keywords as $key => $value) {
            $keyword = new Keyword;
            $keyword->keyword = $value;
            $client->keywords()->save($keyword);
        }
        return redirect('/clients/'.$client->id.'/edit')->with('message', 'Keywords Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        if($client->status == 1) {
            $client->status = 2;
            $client->save();
        }
        else {
            $client->status = 1;
            $client->save();
        }
        return redirect('/clients')->with('message', 'Client Updated');
    }
    
    public function pullsearchdata(Client $client, $password, $start_date, $end_date,$ctr_start, $ctr_end, $imp_start, $imp_end, $pos_start, $pos_end, $keyword_start, $keyword_end, $limit, $sort_variable, $order, $sv_start = NULL, $sv_end = NULL, $country_id = NULL, $num_of_words = 0) {
        
        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));
        $keyword_start = date('Y-m-d', strtotime($keyword_start));
        $keyword_end = date('Y-m-d', strtotime($keyword_end));
        
        ini_set('memory_limit','2000M');
        
        // $this->checkPassword($password);
        $runs = $client->runs()->where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->pluck('id');
        
        $table = array();
        
        foreach(\App\SearchAllData::select(DB::raw("keyword, AVG(ctr) as ctr, AVG(impressions) as impressions, AVG(position) as position, AVG(sv) as sv, client_id"))->whereIn('run_id', $runs)->whereRaw("(LENGTH(keyword) - LENGTH(replace(keyword, ' ', '')) >= $num_of_words)")->where('client_id', $client->id)->groupBy('keyword', 'client_id')->get() as $data) {
            $table[$data->keyword]['ctr'] = $data->ctr;
            $table[$data->keyword]['imp'] = $data->impressions;
            $table[$data->keyword]['pos'] = $data->position;
            $table[$data->keyword]['sv'] = $data->sv;
        }
        
        
        $searchvolumes_other = NULL;
        if($country_id != NULL || $country_id != 0) {
            try {
                $searchvolumes_other = \App\Http\Controllers\SearchVolumeController::get_sv_for_keywords_avg(array_keys($table), [$country_id]);
                foreach($table as $keyword => $array) {
                    if(array_key_exists($keyword, $searchvolumes_other)) {
                        $table[$keyword]['sv'] = $searchvolumes_other[$keyword];
                    }
                    else {
                        $table[$keyword]['sv'] = 0;
                    }
                }
            }
            catch (\Exception $e) {
                $searchvolumes_other = NULL;
            }
        }
        
        
        
        $number_of_keywords = count($table);
        
        $sv = collect($table)->sortBy('sv')->splice($number_of_keywords*$sv_start/100, $number_of_keywords*($sv_end-$sv_start)/100)->toArray();
        $ctr = collect($table)->sortBy('ctr')->splice($number_of_keywords*$ctr_start/100, $number_of_keywords*($ctr_end-$ctr_start)/100)->toArray();
        $imp = collect($table)->sortBy('imp')->splice($number_of_keywords*$imp_start/100, $number_of_keywords*($imp_end-$imp_start)/100)->toArray();
        $pos = collect($table)->where('pos', $this->getOperator($pos_start), $pos_end)->toArray();
        
        $sort_table = array_intersect_key($table, $ctr, $imp, $pos, $sv);
        
        if($order == 1) {
            $sort_table = collect($sort_table)->sortByDesc($sort_variable)->take($limit)->toArray();
        }
        else {
            $sort_table = collect($sort_table)->sortBy($sort_variable)->take($limit)->toArray();
        }
        
        $keywords = array_keys($sort_table);
        
        $runs = $client->runs()->where('start_date', '>=', $keyword_start)->where('end_date', '<=', $keyword_end)->get();
        
        unset($sort_table);
        unset($table);
        unset($data);
        
        $table = array();
        
        foreach($runs as $key => $run) {
            foreach($run->alldata()->whereIn('keyword', $keywords)->get() as $data) {
                $table[$data->keyword][$key]['ctr'] = $data->ctr;
                $table[$data->keyword][$key]['imp'] = $data->impressions;
                $table[$data->keyword][$key]['position'] = $data->position;
                $table[$data->keyword][$key]['sv'] = $data->sv;
            }
        }
        
        
        
        $all_run_keys = array();
        foreach($table as $keyword => $count_array) {
            $all_run_keys = array_merge($all_run_keys, array_keys($count_array));
        }
        $all_run_keys = array_unique($all_run_keys);
        foreach($table as $keyword => $count_array) {
            foreach($all_run_keys as $val) {
                if(!array_key_exists($val, $count_array)) {
                    $table[$keyword][$val]['ctr'] = 0;
                    $table[$keyword][$val]['imp'] = 0;
                    $table[$keyword][$val]['position'] = 0;
                    $table[$keyword][$val]['sv'] = 0;
                }
            }
            
        }
        
        
        //Search Volumes for other countries
        $searchvolumes_other = NULL;
        if($country_id != NULL || $country_id != 0) {
            try {
                $searchvolumes_other = \App\Http\Controllers\SearchVolumeController::get_sv_for_keywords($keywords, [$country_id]);
                foreach($table as $keyword => $array) {
                    foreach($array as $key => $array_2) {
                        $calc_date = date('Y-n', strtotime($runs[$key]->start_date));
                        if($searchvolumes_other != NULL && array_key_exists($calc_date, $searchvolumes_other) && array_key_exists($keyword, $searchvolumes_other[$calc_date])) {
                            $table[$keyword][$key]['sv'] = $searchvolumes_other[$calc_date][$keyword];
                            
                        }
                        else {
                            $table[$keyword][$key]['sv'] = 0;
                        }
                    }
                }
            }
            catch (\Exception $e) {
                $searchvolumes_other = NULL;
            }
        }
        
        $return_array = array();
        $row_array = '';
        
        for($i = 0; $i < count($runs); $i++) {
            $row_array .= ' ,'.$runs[$i]->start_date.','.$runs[$i]->start_date.','.$runs[$i]->start_date.','.$runs[$i]->start_date;
        }
        $return_array[0] = explode(',',$row_array);
        
        $row_array = 'Keyword,';
        for($i = 0; $i < count($runs); $i++) {
            $row_array .= 'CTR,IMP,SV,Position,';
        }
        $return_array[1] = explode(',',$row_array);
        $num = 2;
        foreach($table as $keyword => $row) {
            $row_array = "$keyword,";
            for($i = 0; $i < count($runs); $i++) {
                if(array_key_exists($i, $row)) {
                    $row_array .= $row[$i]['ctr'].",".$row[$i]['imp'].",".$row[$i]['sv'].','.$row[$i]['position'].',';
                }
                else {
                    $row_array .= " , , , ,";
                }
            }
            $return_array[$num] = explode(',',$row_array);
            
            foreach($return_array[$num] as $key => $value) {
                if($key > 0) {
                    $return_array[$num][$key] =  floatval($value);
                }
            }
            $num++;
        }
        
        unset($array);
        foreach($return_array as $k =>$a){
            $array[$k] = json_decode(json_encode($a, JSON_FORCE_OBJECT));
        }
        ksort($array);
        
        return response()->json($array);
        
        // return view('runs.expose', ['runs' => $runs, 'client' => $client->siteUrl, 'table' => $table]);
    }
    
    public function wolf_3(Client $client, $password, $start_date, $end_date, $keywords, $country_id = NULL) {
        
        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));
        
        ini_set('memory_limit','2000M');
        
        $keywords = explode(',', urldecode($keywords));
        $country_id = (int)$country_id;
        try {
            if($country_id != NULL || $country_id != 0) {
                $sv = \App\Http\Controllers\SearchVolumeController::get_sv_for_keywords($keywords, [$country_id]);
            }
            else {
                
                $sv = \App\Http\Controllers\SearchVolumeController::get_sv_for_keywords($keywords);
            }
            
        }
        catch (\Exception $e) {
            $sv = NULL;
        }
        
        $runs = $client->runs()->where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->get();
        
        $table = array();
        $num = 0;
        $num_keys = count($runs);
        foreach($runs as $key => $run) {
            
            $calc_date = date('Y-n', strtotime($run->start_date));
            
            foreach($run->alldata()->whereIn('keyword', $keywords)->get() as $data) {
                
                
                // if($data->sv > 0) {
                //     $table[$data->keyword][$key]['sv'] = $data->sv;
                // }
                // elseif($sv != NULL && array_key_exists($calc_date, $sv) && array_key_exists($data->keyword, $sv[$calc_date])) {
                //     $table[$data->keyword][$key]['sv'] = $sv[$calc_date][$data->keyword];
                // }
                // else {
                //     $table[$data->keyword][$key]['sv'] = 0;
                // }
                
                $table[$data->keyword][$key]['ctr'] = $data->ctr;
                $table[$data->keyword][$key]['imp'] = $data->impressions;
                $table[$data->keyword][$key]['position'] = $data->position;
            }
        }
        
        foreach($keywords as $keyword) {
            if(!array_key_exists($keyword, $table)) {
                for($i = 0; $i < count($runs); $i++) {
                    
                    $table[$keyword][$i]['ctr'] = 0;
                    $table[$keyword][$i]['imp'] = 0;
                    $table[$keyword][$i]['position'] = 0;
                }
            }
            
            for($i = 0; $i < count($runs); $i++) {
                if(array_key_exists($i, $table[$keyword])) {
                    $calc_date = date('Y-n', strtotime($runs[$i]->start_date));
                    if($sv != NULL && array_key_exists($calc_date, $sv) && array_key_exists($keyword, $sv[$calc_date])) {
                        $table[$keyword][$i]['sv'] = $sv[$calc_date][$keyword];
                    }
                    else {
                        $table[$keyword][$i]['sv'] = 0;
                    }
                }
                else {
                    $calc_date = date('Y-n', strtotime($runs[$i]->start_date));
                    if($sv != NULL && array_key_exists($calc_date, $sv) && array_key_exists($keyword, $sv[$calc_date])) {
                        $table[$keyword][$i]['sv'] = $sv[$calc_date][$keyword];
                    }
                    else {
                        $table[$keyword][$i]['sv'] = 0;
                    }
                    $table[$keyword][$i]['ctr'] = 0;
                    $table[$keyword][$i]['imp'] = 0;
                    $table[$keyword][$i]['position'] = 0;
                }
                
            }
        }
        
        $return_array = array();
        $row_array = '';
        
        for($i = 0; $i < count($runs); $i++) {
            $row_array .= ' ,'.$runs[$i]->start_date.','.$runs[$i]->start_date.','.$runs[$i]->start_date.','.$runs[$i]->start_date;
        }
        $return_array[0] = explode(',',$row_array);
        
        $row_array = 'Keyword,';
        for($i = 0; $i < count($runs); $i++) {
            $row_array .= 'CTR,IMP,SV,Position,';
        }
        $return_array[1] = explode(',',$row_array);
        
        $num = 2;
        foreach($table as $keyword => $row) {
            $row_array = "$keyword,";
            for($i = 0; $i < count($runs); $i++) {
                if(array_key_exists($i, $row)) {
                    $row_array .= $row[$i]['ctr'].",".$row[$i]['imp'].",".$row[$i]['sv'].','.$row[$i]['position'].',';
                }
                else {
                    $row_array .= " , , , ,";
                }
            }
            $return_array[$num] = explode(',',$row_array);
            foreach($return_array[$num] as $key => $value) {
                if($key > 0) {
                    $return_array[$num][$key] =  floatval($value);
                }
            }
            $num++;
        }
        
        
        
        foreach($return_array as $k =>$a){
            $array[$k] = json_decode(json_encode($a, JSON_FORCE_OBJECT));
        }
        
        //dd($array);
        
        return response()
            ->json($array);
        
    }
    
    public function getAllClients($password) {
        $this->checkPassword($password);
        $clients = Client::where('status', 1)->get();
        return view('clients.expose', ['clients' => $clients]);
    }
    
    public function getAllCountries($password) {
        $this->checkPassword($password);
        $countries = \App\Country::all();
        return view('clients.countries', ['countries' => $countries]);
    }
    
    public function pull_keyword_data(Client $client, $password, $start_date, $end_date,$ctr_start, $ctr_end, $imp_start, $imp_end, $pos_start, $pos_end, $keyword_start, $keyword_end, $limit, $sort_variable, $order, $sv_start = NULL, $sv_end = NULL, $country_id = NULL, $num_of_words = 0) {
        
        $start_date = date('Y-m-d', strtotime($start_date));
        $end_date = date('Y-m-d', strtotime($end_date));
        $keyword_start = date('Y-m-d', strtotime($keyword_start));
        $keyword_end = date('Y-m-d', strtotime($keyword_end));
        
        ini_set('memory_limit','2000M');
        
        // $this->checkPassword($password);
        $runs = $client->runs()->where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->pluck('id');
        
        $table = array();
        
        foreach(\App\SearchAllData::select(DB::raw("keyword, AVG(ctr) as ctr, AVG(impressions) as impressions, AVG(position) as position, AVG(sv) as sv, client_id"))->whereIn('run_id', $runs)->whereRaw("(LENGTH(keyword) - LENGTH(replace(keyword, ' ', '')) >= $num_of_words)")->where('client_id', $client->id)->groupBy('keyword', 'client_id')->get() as $data) {
            $table[$data->keyword]['ctr'] = $data->ctr;
            $table[$data->keyword]['imp'] = $data->impressions;
            $table[$data->keyword]['pos'] = $data->position;
            $table[$data->keyword]['sv'] = $data->sv;
        }
        
        
        $searchvolumes_other = NULL;
        if($country_id != NULL || $country_id != 0) {
            try {
                $searchvolumes_other = \App\Http\Controllers\SearchVolumeController::get_sv_for_keywords_avg(array_keys($table), [$country_id]);
                foreach($table as $keyword => $array) {
                    if(array_key_exists($keyword, $searchvolumes_other)) {
                        $table[$keyword]['sv'] = $searchvolumes_other[$keyword];
                    }
                    else {
                        $table[$keyword]['sv'] = 0;
                    }
                }
            }
            catch (\Exception $e) {
                $searchvolumes_other = NULL;
            }
        }
        
        
        
        $number_of_keywords = count($table);
        
        $sv = collect($table)->sortBy('sv')->splice($number_of_keywords*$sv_start/100, $number_of_keywords*($sv_end-$sv_start)/100)->toArray();
        $ctr = collect($table)->sortBy('ctr')->splice($number_of_keywords*$ctr_start/100, $number_of_keywords*($ctr_end-$ctr_start)/100)->toArray();
        $imp = collect($table)->sortBy('imp')->splice($number_of_keywords*$imp_start/100, $number_of_keywords*($imp_end-$imp_start)/100)->toArray();
        $pos = collect($table)->where('pos', $this->getOperator($pos_start), $pos_end)->toArray();
        
        $sort_table = array_intersect_key($table, $ctr, $imp, $pos, $sv);
        
        if($order == 1) {
            $sort_table = collect($sort_table)->sortByDesc($sort_variable)->take($limit)->toArray();
        }
        else {
            $sort_table = collect($sort_table)->sortBy($sort_variable)->take($limit)->toArray();
        }
        
        $keywords = array_keys($sort_table);
        
        $runs = $client->runs()->where('start_date', '>=', $start_date)->where('end_date', '<=', $end_date)->pluck('id');
        
        unset($sort_table);
        unset($table);
        unset($data);
        
        $table = array();
        
        if($order == 1) {
            $order = 'desc';
        }
        else {
            $order = 'asc';
        }
        switch ($sort_variable) {
            case 'ctr':
                $sort_variable = 'ctr';
                break;
            case 'pos':
                $sort_variable = 'position';
                break;
            case 'sv':
                $sort_variable = 'sv';
                break;
            case 'imp':
                $sort_variable = 'impressions';
                break;
            
            default:
                $sort_variable = 'ctr';
                break;
        }

        foreach(\App\SearchAllData::select(DB::raw("keyword, AVG(ctr) as ctr, AVG(impressions) as impressions, AVG(position) as position, AVG(sv) as sv, client_id"))->whereIn('keyword', $keywords)->whereIn('run_id', $runs)->where('client_id', $client->id)->groupBy('keyword', 'client_id')->orderBy($sort_variable, $order)->get() as $key => $data) {
            $table[$data->keyword][$key]['ctr'] = $data->ctr;
            $table[$data->keyword][$key]['imp'] = $data->impressions;
            $table[$data->keyword][$key]['position'] = $data->position;
            $table[$data->keyword][$key]['sv'] = $data->sv;
        }
        
        if($searchvolumes_other != NULL) {
            foreach($table as $keyword => $array) {
                if(array_key_exists($keyword, $searchvolumes_other)) {
                    foreach($array as $key => $var) {
                        $table[$keyword][$key]['sv'] = $searchvolumes_other[$keyword];
                    }
                }
            }
        }
        
        $return_array = array();
        $row_array = '';
        
        // for($i = 0; $i < count($runs); $i++) {
        //     $row_array .= ' ,'.$runs[$i]->start_date.', , , ';
        // }
        // $return_array[0] = explode(',', ",,,,,");
        
        $row_array = 'Keyword,';
        for($i = 0; $i < 1; $i++) {
            $row_array .= 'CTR,IMP,SV,Position,';
        }
        $return_array[0] = explode(',',$row_array);
        
        $num = 1;
        foreach($table as $keyword => $row) {
            $row_array = "$keyword,";
            foreach($row as $key => $data_var) {
                $row_array .= $data_var['ctr'].",".$data_var['imp'].",".$data_var['sv'].','.$data_var['position'].',';
            }
            
            $return_array[$num] = explode(',',$row_array);
            foreach($return_array[$num] as $key => $value) {
                if($key > 0) {
                    $return_array[$num][$key] =  floatval($value);
                }
            }
            $num++;
        }
        
        unset($array);
        foreach($return_array as $k =>$a){
            $array[$k] = json_decode(json_encode($a, JSON_FORCE_OBJECT));
        }
        
        //dd($array);
        
        return response()
            ->json($array);
        
        //return view('runs.expose', ['runs' => $runs, 'client' => $client->siteUrl, 'table' => $table]);
    }
    
    public function test() {
        // $client = $this->service_account();
        // $service = new \Google_Service_Webmasters($client);
        // $search = new \Google_Service_Webmasters_SearchAnalyticsQueryRequest;
        // $search->setStartDate( date( 'Y-m-d', strtotime( '2018-05-10' ) ) );
        // $search->setEndDate( date( 'Y-m-d', strtotime( '2018-07-10' ) ) );
        // $search->setDimensions(['query']);
        // $search->setRowLimit(25000);
        
        // $results = $service->searchanalytics->query( 'https://www.cwcycles.co.za/', $search )->getRows();
        // $keywords = ['hello kitty', 'bob the drag queen'];
        // dd();
    }
    
    public function checkPassword($password) {
        if(decrypt($password) != 'AngryDragon') {
            exit;
        }
    }
    
    public function getOperator($var) {
        $operators[0] = "=";
        $operators[1] = ">";
        $operators[2] = "<";
        $operators[3] = ">=";
        $operators[4] = "<=";
        return $operators[$var];
    }
}
