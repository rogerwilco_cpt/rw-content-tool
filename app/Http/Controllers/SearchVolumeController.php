<?php

namespace App\Http\Controllers;

use App\SearchVolume;
use Illuminate\Http\Request;
use App\Jobs\GetSearchVolumes;


use Google\AdsApi\AdWords\AdWordsServices;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\v201809\cm\Language;
use Google\AdsApi\AdWords\v201809\cm\NetworkSetting;
use Google\AdsApi\AdWords\v201809\cm\Paging;
use Google\AdsApi\AdWords\v201809\o\AttributeType;
use Google\AdsApi\AdWords\v201809\o\IdeaType;
use Google\AdsApi\AdWords\v201809\o\LanguageSearchParameter;
use Google\AdsApi\AdWords\v201809\o\NetworkSearchParameter;
use Google\AdsApi\AdWords\v201809\o\RelatedToQuerySearchParameter;
use Google\AdsApi\AdWords\v201809\o\RequestType;
use Google\AdsApi\AdWords\v201809\o\SeedAdGroupIdSearchParameter;
use Google\AdsApi\AdWords\v201809\o\TargetingIdeaSelector;
use Google\AdsApi\AdWords\v201809\o\MonthlySearchVolume;
use Google\AdsApi\AdWords\v201809\o\TargetingIdeaService;
use Google\AdsApi\Common\OAuth2TokenBuilder;
use Google\AdsApi\Common\Util\MapEntries;
use Google\AdsApi\AdWords\v201809\o\LocationSearchParameter;
use Google\AdsApi\AdWords\v201809\cm\Location;

class SearchVolumeController extends Controller
{
    
    protected $session;
    protected $adwordsservices;
    protected $result_array = array();
    protected $keywords = array();
    
    public function __construct() {
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $this->get_all_keywords();
        
    }
    
    public function process_page_entries($page) {
        $result_array = array();
        $current_keyword = '';
        
        foreach($page->getEntries() as $targetingIdeaEntry) {
            foreach($targetingIdeaEntry->getData() as $data) {
                $key = $data->getKey();
                $value = $data->getValue()->getValue();
                
                if($key == 'KEYWORD_TEXT') {
                    $current_keyword = $value;
                }
                elseif($key == 'TARGETED_MONTHLY_SEARCHES') {
                    $current_data = $value;
                }
                
            }
            
            foreach($current_data as $MonthlySearchVolume) {
                $result_array[$MonthlySearchVolume->getYear().'-'.$MonthlySearchVolume->getMonth()][$current_keyword] = $MonthlySearchVolume->getCount();
            }
            
        }
        
        $this->result_array = array_merge($result_array, $this->result_array);
        
    }
    
    public function process_result_array() {
        foreach($this->result_array as $date => $arr) {
            $this->save_keywords_volumes_by_date($date);
        }
    }
    
    public function save_keywords_volumes_by_date($date) {
        $query_date = date('Y-m', strtotime($date));
        
        $runs = \App\Run::where('start_date', 'like', "$query_date%")->pluck('id')->toArray();
        $data = \App\SearchAllData::whereIn('run_id', $runs)->where('sv', '<', 1)->distinct()->get();
        
        foreach($data as $row) {
            if(array_key_exists($row->keyword, $this->result_array[$date])) {
                if($this->result_array[$date][$row->keyword] != NULL) {
                    $row->sv = $this->result_array[$date][$row->keyword];
                    $row->save();
                }
                else {
                    $row->sv = 0;
                    $row->save();
                }
                
            }
            
        }
        
    }
    
    public function get_all_keywords() {
        
        $start_date = date('Y-m', strtotime('-1 years'));
        
        $run_ids = \App\Run::where('start_date', '>=', $start_date)->pluck('id')->toArray();
        
        $minutes = 2;
        
        foreach(\App\Client::where('status', 1)->get() as $client) {
            if(($client->countries)->isNotEmpty()) {
                $countries = $client->countries->pluck('gid')->toArray();
            }
            else {
                $countries = NULL;
            }
            
            $data = \App\SearchAllData::select('keyword')->where('client_id', $client->id)->whereIn('run_id', $run_ids)->where('sv', '<', 1)->groupBy('keyword')->get();
        
            foreach($data->chunk(700) as $keywords) {
                GetSearchVolumes::dispatch($keywords->pluck('keyword')->toArray(), $countries)->delay(now()->addMinutes($minutes));
                $minutes = $minutes + 2;
            }
        }
        
        
        
    }
    
    public function run_adwords_query($keywords, $location_ids) {
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile('/var/www/searchdata/adsapi_php.ini')->build();
        // Construct an API session configured from a properties file and the
        // OAuth2 credentials above.
        $session = (new AdWordsSessionBuilder())->fromFile('/var/www/searchdata/adsapi_php.ini')->withOAuth2Credential($oAuth2Credential)->build();
        
        $this->adwordsservices = new AdWordsServices();
        $this->session = $session;
        
        $targetingIdeaService = $this->adwordsservices->get($this->session, TargetingIdeaService::class);
        
        $selector = new TargetingIdeaSelector();
        $selector->setRequestType(RequestType::STATS);
        $selector->setIdeaType(IdeaType::KEYWORD);
        
        $selector->setRequestedAttributeTypes(
            [
                AttributeType::TARGETED_MONTHLY_SEARCHES,
                AttributeType::KEYWORD_TEXT,
            ]
        );
        
        //Add Networks
        $networkSetting = new NetworkSetting();
        $networkSetting->setTargetGoogleSearch(true);
        $networkSetting->setTargetSearchNetwork(false);
        $networkSetting->setTargetContentNetwork(false);
        $networkSetting->setTargetPartnerSearchNetwork(false);

        $networkSearchParameter = new NetworkSearchParameter();
        $networkSearchParameter->setNetworkSetting($networkSetting);
        
        $paging = new Paging();
        $paging->setStartIndex(0);
        $paging->setNumberResults(10000);
        $selector->setPaging($paging);
        
        $searchParameters = [];
        // Create related to query search parameter.
        $relatedToQuerySearchParameter = new RelatedToQuerySearchParameter();
        $relatedToQuerySearchParameter->setQueries($keywords);
        
        
        $searchParameters[] = $relatedToQuerySearchParameter;
        $searchParameters[] = $networkSearchParameter;
        
        //Add country
        if($location_ids != NULL) {
            foreach($location_ids as $location_id) {
                $country = new Location();
                $country->setId($location_id);
                $countries[] = $country;
            }
            $locationSearchParameter = new LocationSearchParameter();
            $locationSearchParameter->setLocations($countries);
            $searchParameters[] = $locationSearchParameter;
        }
        else {
            $country = new Location();
            $country->setId('2710');
            $countries[] = $country;
            $locationSearchParameter = new LocationSearchParameter();
            $locationSearchParameter->setLocations($countries);
            $searchParameters[] = $locationSearchParameter;
        }
        
        
        
        $selector->setSearchParameters($searchParameters);
        
        $page = $targetingIdeaService->get($selector);
        
        $this->process_page_entries($page);
        $this->process_result_array();
        
    }
    
    public static function get_sv_for_keywords($keywords, $location_ids = NULL) {
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile('/var/www/searchdata/adsapi_php.ini')->build();
        // Construct an API session configured from a properties file and the
        // OAuth2 credentials above.
        $session = (new AdWordsSessionBuilder())->fromFile('/var/www/searchdata/adsapi_php.ini')->withOAuth2Credential($oAuth2Credential)->build();
        
        $adwordsservices = new AdWordsServices();
        
        $targetingIdeaService = $adwordsservices->get($session, TargetingIdeaService::class);
        
        $selector = new TargetingIdeaSelector();
        $selector->setRequestType(RequestType::STATS);
        $selector->setIdeaType(IdeaType::KEYWORD);
        
        $selector->setRequestedAttributeTypes(
            [
                AttributeType::TARGETED_MONTHLY_SEARCHES,
                AttributeType::KEYWORD_TEXT,
            ]
        );
        
        //Add Networks
        $networkSetting = new NetworkSetting();
        $networkSetting->setTargetGoogleSearch(true);
        $networkSetting->setTargetSearchNetwork(false);
        $networkSetting->setTargetContentNetwork(false);
        $networkSetting->setTargetPartnerSearchNetwork(false);

        $networkSearchParameter = new NetworkSearchParameter();
        $networkSearchParameter->setNetworkSetting($networkSetting);
        
        $paging = new Paging();
        $paging->setStartIndex(0);
        $paging->setNumberResults(10000);
        $selector->setPaging($paging);
        
        $searchParameters = [];
        // Create related to query search parameter.
        $relatedToQuerySearchParameter = new RelatedToQuerySearchParameter();
        $relatedToQuerySearchParameter->setQueries($keywords);
        $searchParameters[] = $relatedToQuerySearchParameter;
        $searchParameters[] = $networkSearchParameter;
        
        //Add country
        if($location_ids != NULL) {
            foreach($location_ids as $location_id) {
                $country = new Location();
                $country->setId($location_id);
                $countries[] = $country;
            }
            $locationSearchParameter = new LocationSearchParameter();
            $locationSearchParameter->setLocations($countries);
            $searchParameters[] = $locationSearchParameter;
        }
        else {
            $country = new Location();
            $country->setId('2710');
            $countries[] = $country;
            $locationSearchParameter = new LocationSearchParameter();
            $locationSearchParameter->setLocations($countries);
            $searchParameters[] = $locationSearchParameter;
        }
        
        $selector->setSearchParameters($searchParameters);
        
        $page = $targetingIdeaService->get($selector);
        
        $result_array = array();
        $current_keyword = '';
        
        foreach($page->getEntries() as $targetingIdeaEntry) {
            foreach($targetingIdeaEntry->getData() as $data) {
                $key = $data->getKey();
                $value = $data->getValue()->getValue();
                
                if($key == 'KEYWORD_TEXT') {
                    $current_keyword = $value;
                }
                elseif($key == 'TARGETED_MONTHLY_SEARCHES') {
                    $current_data = $value;
                }
                
            }
            
            foreach($current_data as $MonthlySearchVolume) {
                $result_array[$MonthlySearchVolume->getYear().'-'.$MonthlySearchVolume->getMonth()][$current_keyword] = $MonthlySearchVolume->getCount();
            }
            
        }
        
        return $result_array;
    }
    
    public static function get_sv_for_keywords_avg($keywords, $location_ids = NULL) {
        $oAuth2Credential = (new OAuth2TokenBuilder())->fromFile('/var/www/searchdata/adsapi_php.ini')->build();
        // Construct an API session configured from a properties file and the
        // OAuth2 credentials above.
        $session = (new AdWordsSessionBuilder())->fromFile('/var/www/searchdata/adsapi_php.ini')->withOAuth2Credential($oAuth2Credential)->build();
        
        $adwordsservices = new AdWordsServices();
        
        $targetingIdeaService = $adwordsservices->get($session, TargetingIdeaService::class);
        
        $selector = new TargetingIdeaSelector();
        $selector->setRequestType(RequestType::STATS);
        $selector->setIdeaType(IdeaType::KEYWORD);
        
        $selector->setRequestedAttributeTypes(
            [
                AttributeType::SEARCH_VOLUME,
                AttributeType::KEYWORD_TEXT,
            ]
        );
        
        //Add Networks
        $networkSetting = new NetworkSetting();
        $networkSetting->setTargetGoogleSearch(true);
        $networkSetting->setTargetSearchNetwork(false);
        $networkSetting->setTargetContentNetwork(false);
        $networkSetting->setTargetPartnerSearchNetwork(false);

        $networkSearchParameter = new NetworkSearchParameter();
        $networkSearchParameter->setNetworkSetting($networkSetting);
        
        $paging = new Paging();
        $paging->setStartIndex(0);
        $paging->setNumberResults(10000);
        $selector->setPaging($paging);
        
        $searchParameters = [];
        // Create related to query search parameter.
        $relatedToQuerySearchParameter = new RelatedToQuerySearchParameter();
        $relatedToQuerySearchParameter->setQueries($keywords);
        $searchParameters[] = $relatedToQuerySearchParameter;
        $searchParameters[] = $networkSearchParameter;
        
        //Add country
        if($location_ids != NULL) {
            foreach($location_ids as $location_id) {
                $country = new Location();
                $country->setId($location_id);
                $countries[] = $country;
            }
            $locationSearchParameter = new LocationSearchParameter();
            $locationSearchParameter->setLocations($countries);
            $searchParameters[] = $locationSearchParameter;
        }
        else {
            $country = new Location();
            $country->setId('2710');
            $countries[] = $country;
            $locationSearchParameter = new LocationSearchParameter();
            $locationSearchParameter->setLocations($countries);
            $searchParameters[] = $locationSearchParameter;
        }
        
        $selector->setSearchParameters($searchParameters);
        
        $page = $targetingIdeaService->get($selector);
        
        $result_array = array();
        $current_keyword = '';
        
        foreach($page->getEntries() as $targetingIdeaEntry) {
            foreach($targetingIdeaEntry->getData() as $data) {
                $key = $data->getKey();
                $value = $data->getValue()->getValue();
                
                if($key == 'KEYWORD_TEXT') {
                    $current_keyword = $value;
                }
                elseif($key == 'SEARCH_VOLUME') {
                    $current_data = $value;
                }
                
            }

            $result_array[$current_keyword] = $current_data;
            
        }
        
        return $result_array;
    }
    
    
}
