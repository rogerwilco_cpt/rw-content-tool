<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientCountryController extends Controller
{
    public function edit(\App\Client $client) {
        $return['client'] = $client;
        
        return view('clients/country', $return);
    }
    
    public function add(\App\Client $client) {
        $return['client'] = $client;
        $return['countries'] = \App\Country::select('id', 'name')->get();
        
        return view('clients/countryadd', $return);
    }
    
    public function save(Request $request, \App\Client $client) {
        $country = \App\Country::where('name', $request->input('country'))->firstOrFail();
        
        $link = new \App\ClientCountry;
        $link->country_id = $country->id;
        $link->client_id = $client->id;
        $link->save();
        
        return redirect('/country/'.$client->id);
        
    }
    
    public function delete(\App\Client $client, \App\Country $country) {
        $link = \App\ClientCountry::where('client_id', $client->id)->where('country_id', $country->id)->first();
        $link->delete();
        return redirect('/country/'.$client->id);
    }
}
