<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::group(['middleware' => 'ipcheck'], function() {
    Route::get('/', function () {
        return view('welcome');
    });
    Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');
    
});

Route::resource('clients', 'ClientController')->middleware('auth');
Route::resource('runs', 'RunController')->middleware('auth');
Route::resource('settings', 'SettingController')->middleware('auth');
Route::get('/pullsearchdata/{client}/{password}/{start_date}/{end_date}/{ctr_start}/{ctr_end}/{imp_start}/{imp_end}/{pos_start}/{pos_end}/{keyword_start}/{keyword_end}/{limit}/{sort_variable}/{order}/{sv_start}/{sv_end}/{country_id}/{num_of_words}', 'ClientController@pullsearchdata');
Route::get('/pullkeyworddata/{client}/{password}/{start_date}/{end_date}/{ctr_start}/{ctr_end}/{imp_start}/{imp_end}/{pos_start}/{pos_end}/{keyword_start}/{keyword_end}/{limit}/{sort_variable}/{order}/{sv_start}/{sv_end}/{country_id}/{num_of_words}', 'ClientController@pull_keyword_data');
Route::get('/getallclients/{password}', 'ClientController@getAllClients');
Route::get('/getallcountries/{password}', 'ClientController@getAllCountries');
Route::get('/pullsearchvolumes', 'SearchVolumeController@index')->middleware('auth');
Route::get('/wolf_3/{client}/{password}/{start_date}/{end_date}/{keywords}/{country_id}', 'ClientController@wolf_3');

Route::get('/country/{client}', 'ClientCountryController@edit');
Route::get('/country/{client}/add', 'ClientCountryController@add');
Route::get('/country/{client}/delete/{country}', 'ClientCountryController@delete');
Route::post('/country/{client}/add', 'ClientCountryController@save');

Route::get('/test', 'ClientController@test');